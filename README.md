# TP1 Android

## Exercice 3
1. [Layout XML](https://gitlab.com/Romimap/tp1-android/-/blob/master/TP1/app/src/main/res/layout/activity_main.xml)
2. [Layout code](https://gitlab.com/Romimap/tp1-android/-/blob/master/TP1/app/src/main/java/com/example/tp1/MainActivity.java)
3. Informations sur les champs:
> XML: android:hint = "Indice"

> Code: vue.setHint("Indice");
   
## Exercice 4
1. [strings.xml](https://gitlab.com/Romimap/tp1-android/-/blob/master/TP1/app/src/main/res/values/strings.xml) par défaut
2. [strings.xml](https://gitlab.com/Romimap/tp1-android/-/blob/master/TP1/app/src/main/res/values-fr/strings.xml) francais

## Exercice 5
1. [code](https://gitlab.com/Romimap/tp1-android/-/blob/master/TP1/app/src/main/java/com/example/tp1/MainActivity.java)

## Exercice 6
1. [code](https://gitlab.com/Romimap/tp1-android/-/blob/master/TP1/app/src/main/java/com/example/tp1/PrintInfo.java)
2. [xml](https://gitlab.com/Romimap/tp1-android/-/blob/master/TP1/app/src/main/res/layout/activity_print_info.xml)

## Exercice 7
1. [code](https://gitlab.com/Romimap/tp1-android/-/blob/master/TP1/app/src/main/java/com/example/tp1/CallActivity.java)
2. [xml](https://gitlab.com/Romimap/tp1-android/-/blob/master/TP1/app/src/main/res/layout/activity_call.xml)

## Exercice 8
1. [code (Choix départ arrivée)](https://gitlab.com/Romimap/tp1-android/-/blob/master/Exercice8/app/src/main/java/com/example/exercice8/ChooseDepartureDestination.java)
2. [xml (Choix départ arrivée)](https://gitlab.com/Romimap/tp1-android/-/blob/master/Exercice8/app/src/main/res/layout/activity_choose_departure_destination.xml)
3. [code (Affichage Liste)](https://gitlab.com/Romimap/tp1-android/-/blob/master/Exercice8/app/src/main/java/com/example/exercice8/PrintTravels.java)
4. [xml (Affichage Liste)](https://gitlab.com/Romimap/tp1-android/-/blob/master/Exercice8/app/src/main/res/layout/activity_print_travels.xml)
5. [Ressource Tableau](https://gitlab.com/Romimap/tp1-android/-/blob/master/Exercice8/app/src/main/res/values/arrays.xml)


# TD1 Android

## Exercice 1 : Hello Android
[JAVA](https://gitlab.com/Romimap/tp1-android/-/blob/master/TD1/app/src/main/java/com/example/td1/HelloAndroid.java)

[XML](https://gitlab.com/Romimap/tp1-android/-/blob/master/TD1/app/src/main/res/layout/activity_hello_android.xml)


## Exercice 2 : Les Ressources
[JAVA](https://gitlab.com/Romimap/tp1-android/-/blob/master/TD1/app/src/main/java/com/example/td1/LesRessources.java)

[XML](https://gitlab.com/Romimap/tp1-android/-/blob/master/TD1/app/src/main/res/layout/activity_les_ressources.xml)


## Exercice 3 : Les Layouts
[JAVA](https://gitlab.com/Romimap/tp1-android/-/blob/master/TD1/app/src/main/java/com/example/td1/LesLayouts.java)

[XML 1](https://gitlab.com/Romimap/tp1-android/-/blob/master/TD1/app/src/main/res/layout/activity_les_layouts_1.xml)

[XML 2](https://gitlab.com/Romimap/tp1-android/-/blob/master/TD1/app/src/main/res/layout/activity_les_layouts_2.xml)

[XML 3](https://gitlab.com/Romimap/tp1-android/-/blob/master/TD1/app/src/main/res/layout/activity_les_layouts_3.xml)


## Exercice 4 : Evènements utilisateurs
[JAVA](https://gitlab.com/Romimap/tp1-android/-/blob/master/TD1/app/src/main/java/com/example/td1/Evenements.java)

[XML](https://gitlab.com/Romimap/tp1-android/-/blob/master/TD1/app/src/main/res/layout/activity_evenements.xml)


## Exercice 5 : Différentes vues et actions
[JAVA](https://gitlab.com/Romimap/tp1-android/-/blob/master/TD1/app/src/main/java/com/example/td1/Actions.java)

[XML](https://gitlab.com/Romimap/tp1-android/-/blob/master/TD1/app/src/main/res/layout/activity_actions.xml)
