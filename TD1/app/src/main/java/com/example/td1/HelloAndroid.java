package com.example.td1;

import android.app.Activity;
import android.os.Bundle;
import android.text.Layout;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class HelloAndroid extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_hello_android); //(cf activity_hello_android.xml)

        /* LAYOUT EN CODE

        LinearLayout ll = new LinearLayout(this);
        TextView tv = new TextView(this);
        tv.setText("Hello, Android");
        ll.addView(tv);
        EditText et = new EditText(this); //Exercice 1 Question 4, code
        ll.addView(et);

        setContentView(ll);
         */
    }
}