package com.example.td1;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class Evenements extends Activity {
    private Button button, buttonLongPress;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_evenements);
        button = (Button)findViewById(R.id.mainbutton);
        button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Toast.makeText(getApplicationContext(),"MessageBouton 1", Toast.LENGTH_LONG).show();
            }
        });

        buttonLongPress = (Button)findViewById(R.id.longPressButton);
        buttonLongPress.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                TextView tv = (TextView)findViewById(R.id.textViewToChange);
                if (tv.getText().equals("A")) {
                    tv.setText("B");
                } else {
                    tv.setText("A");
                }
                return true;
            }
        });
    }
}