package com.example.exercice8;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.Random;

public class PrintTravels extends AppCompatActivity {

    Random r = new Random();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_print_travels);

        Intent intent = getIntent();

        TextView leftText = findViewById(R.id.LeftText);
        leftText.setText((CharSequence)intent.getExtras().get("departure"));
        TextView rightText = findViewById(R.id.RightText);
        rightText.setText((CharSequence)intent.getExtras().get("arrival"));

        LinearLayout Left = findViewById(R.id.listLeft);
        LinearLayout Right = findViewById(R.id.listRight);


        for (int h = 8; h < 20; h += (r.nextInt(3) + 1)) { //Fake entries
            int h2 = h + r.nextInt(3) + 1;
            int m = r.nextInt(50) + 10;
            int m2 = r.nextInt(50) + 10;

            TextView itemLeft = new TextView(this);
            String itemLeftStr = h + "h" + m;
            itemLeft.setText(itemLeftStr);
            Left.addView(itemLeft);

            TextView itemRight = new TextView(this);
            String itemRightStr = h2 + "h" + m2;
            itemRight.setText(itemRightStr);
            Right.addView(itemRight);
        }

        Button button = findViewById(R.id.cancel);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
}