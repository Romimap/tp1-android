package com.example.exercice8;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Spinner;

public class ChooseDepartureDestination extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_departure_destination);
        Spinner departure = findViewById(R.id.departure);
        Spinner arrival = findViewById(R.id.arrival);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.destinations, R.layout.support_simple_spinner_dropdown_item);

        departure.setAdapter(adapter);
        arrival.setAdapter(adapter);

        Button button = findViewById(R.id.submitButton);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), PrintTravels.class);
                i.putExtra("departure", departure.getSelectedItem().toString());
                i.putExtra("arrival", arrival.getSelectedItem().toString());
                startActivity(i);
            }
        });
    }
}