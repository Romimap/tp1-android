package com.example.tp1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class PrintInfo extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_print_info);
        Intent intent = getIntent();

        TextView firstName = findViewById(R.id.firstName);
        firstName.setText((CharSequence)intent.getExtras().get("firstName"));

        TextView lastName = findViewById(R.id.lastName);
        lastName.setText((CharSequence)intent.getExtras().get("lastName"));

        TextView skill = findViewById(R.id.skill);
        skill.setText((CharSequence)intent.getExtras().get("skill"));

        TextView age = findViewById(R.id.age);
        age.setText((CharSequence)intent.getExtras().get("age"));

        TextView phone = findViewById(R.id.phone);
        phone.setText((CharSequence)intent.getExtras().get("phone"));

        Button okButton = findViewById(R.id.okButton);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), CallActivity.class);
                i.putExtra("phone", (CharSequence)intent.getExtras().get("phone"));
                startActivity(i);
            }
        });
    }
}