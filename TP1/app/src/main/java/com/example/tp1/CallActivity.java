package com.example.tp1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class CallActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call);

        Intent intent = getIntent();

        TextView phone = (TextView)findViewById(R.id.phone);
        phone.setText((CharSequence)intent.getExtras().get("phone"));

        Button b = (Button)findViewById(R.id.callButton);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + intent.getExtras().get("phone"))); //ACTION_CALL doesn't seem to work on my emulator
                startActivity(i);
            }
        });

    }
}