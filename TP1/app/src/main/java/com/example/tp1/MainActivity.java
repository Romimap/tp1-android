package com.example.tp1;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableLayout;
import android.widget.TableRow;

public class MainActivity extends AppCompatActivity {
    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        // *** *** Exercice 3b

        setContentView(R.layout.activity_main);



        // *** *** Exercice 3c

//      // *** Racine
//
//      ConstraintLayout cl = new ConstraintLayout(this);
//
//      // *** Attacher un enfant à son parent
//
//      TableLayout tl = new TableLayout(this);
//      cl.addView(tl);
//
//      // *** Parametrer une vue
//
//      TableRow tr = new TableRow(this);
//      tl.addView(tr);
//      TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT);
//      lp.setMargins(10, 10, 10, 10);
//      tr.setLayoutParams(lp);
//
//      // *** Recuperer une valeur des ressources
//
//      EditText et = new EditText(this);
//      et.setHint(R.string.firstName);
//      tr.addView(et);
//
//      //ETC...
//
//      // *** Afficher le tout
//
//      setContentView(cl);


        // *** *** Exercice 3d, setHint("Hint") ou android:hint="Hint"

        button = (Button)findViewById(R.id.submit);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog();
            }
        });

    }

    // *** *** Exercice 5
    public void openDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(MainActivity.this);
        dialog.setCancelable(false);
        dialog.setTitle(R.string.alertTitle);
        dialog.setMessage(R.string.alertMessage);
        dialog.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                Intent intent = new Intent(getApplicationContext(), PrintInfo.class);

                EditText firstName = (EditText)findViewById(R.id.firstName);
                intent.putExtra("firstName", firstName.getText());

                EditText lastName = (EditText)findViewById(R.id.lastName);
                intent.putExtra("lastName", lastName.getText());

                EditText age = (EditText)findViewById(R.id.age);
                intent.putExtra("age", age.getText());

                EditText phone = (EditText)findViewById(R.id.phone);
                intent.putExtra("phone", phone.getText());

                RadioGroup radioGroup = (RadioGroup)findViewById(R.id.skills);
                RadioButton skill = (RadioButton)findViewById(radioGroup.getCheckedRadioButtonId());
                intent.putExtra("skill", skill.getText());

                startActivity(intent);
            }
        })
        .setNegativeButton(android.R.string.no, null);

        final AlertDialog alert = dialog.create();
        alert.show();
    }
}
